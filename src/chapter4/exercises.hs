module Exercises where

len :: [a] -> Integer
len (x:xs) = 1 + len xs
len _      = 0
