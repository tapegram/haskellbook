module Reverse where

rvrs :: String -> String
rvrs (x:xs) = rvrs xs ++ [x]
rvrs x      = x

main :: IO ()
main = print $ rvrs "Curry is awesome"
