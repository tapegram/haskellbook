module Learn where

-- x = 10 * 5 + y
-- myResult = x * 5
-- y = 10

-- Exercises: Heal the sick
area x = 3.14 * (x * x)
double x = x * 2
x = 7
y = 10
f = x + y

-- Let and Where
printInc n = print plusTwo
    where plusTwo = n + 2

printInc2 n =
    let plusTwo = n + 2
    in print plusTwo

-- Exercises: A head code
-- let x = 5 in x -- 5
-- let x = 5 in x * x -- 25
-- let x = 5; y = 6 in x * y -- 30
-- let x = 3; y = 1000 in x + 3 -- 6

mult1       = x * y
    where x = 5
          y = 6
