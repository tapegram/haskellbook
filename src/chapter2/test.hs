sayHello :: String -> IO ()
sayHello x = 
    putStrLn ("Hello " ++ x ++ "!")

triple x = x * 3
square x = x * x

areaOfCircle :: Float -> Floatk
areaOfCircle radius = pi * (square radius)

-- Indentation Fun
foo x =
    let 
        y = x * 2
        z = x ^2
    in 2 * y * z